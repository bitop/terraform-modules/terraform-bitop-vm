terraform {
  required_providers {
    vsphere = {
      source  = "hashicorp/vsphere"
      version = "2.0.2"
    }
  }
}

provider "vsphere" {
  user                 = "administrator@thebitop.net"
  password             = "51l0p0rt3M.01"
  vsphere_server       = "vsphere.thebitop.net"
  allow_unverified_ssl = true
}

module "cloud_init" {
  source         = "../"
  vm_depends_on  = []
  tag_depends_on = []
  instances      = 2

  # vsphere
  datacenter        = "bitop"
  resource_pool     = "thebitop/Resources"
  datastore_cluster = ""
  datastore         = ""

  # VM Info
  vm_name        = "consul-svr"
  vm_name_format = "%03d"
  vm_template    = "linux-ubuntu-server-20-04"
  vm_folder      = "GitLab"
  annotation     = "Going of the deep end"
  firmware       = "efi"

  # VM Network
  network        = ["VM Network"]
  cidr           = "192.168.100.0/24"
  use_dhcp       = true
  interface_name = "ens5"
  network_type   = ["vmxnet3"]
  ip_addr        = ["192.168.100.20"]
  ip_netmask     = ["24"]
  ip_gateway     = "192.168.100.1"
  dns_servers    = ["8.8.8.8", "1.1.1.1", "192.168.10.6"]

  # OS Disks
  disk_size_gb      = ["80"]
  disk_datastore    = "esxi-01"
  storage_policy_id = null

  # Data Disks
  data_disk = {
    disk1 = {
      size_gb                   = 30,
      thin_provisioned          = true,
      data_disk_scsi_controller = 0,
      storage_policy_id         = "ff45cc66-b624-4621-967f-1aef6437f568"
    }
  }
  disk_label = []

  # Disk Options
  io_reservation             = []
  io_share_level             = ["normal"]
  io_share_count             = []
  disk_mode                  = []
  template_storage_policy_id = []
  scsi_bus_sharing           = "physicalSharing"
  scsi_type                  = "pvscsi"
  scsi_controller            = 0
  enable_disk_uuid           = true

  # VM resources
  cpu_number             = 2
  cpu_cores_per_socket   = 2
  cpu_reservation        = 4
  cpu_share_level        = "normal"
  cpu_hot_add_enabled    = true
  cpu_hot_remove_enabled = true
  memory_size            = 4096
  memory_reservation     = 2048
  memory_share_level     = "normal"
  memory_hot_add_enabled = true

  # Cloud Init
  is_cloud_init   = true
  cloud_init_file = "path/to/file"
  cloud_init_data = data.template.cloud_init

  # Extra/Global
  wait_for_guest_net_routable      = true
  wait_for_guest_ip_timeout        = 0
  wait_for_guest_net_timeout       = 5
  ignored_guest_ips                = []
  hv_mode                          = null
  ept_rvi_mode                     = null
  nested_hv_enabled                = false
  enable_logging                   = true
  cpu_performance_counters_enabled = true
  swap_placement_policy            = null
  latency_sensitivity              = null
  shutdown_wait_timeout            = 5
  migrate_wait_timeout             = 5
  force_power_off                  = true
}