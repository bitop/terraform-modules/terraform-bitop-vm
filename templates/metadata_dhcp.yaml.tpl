local-hostname: ${hostname}
instance-id: ${instance_id}
network:
  version: 2
  ethernets:
    ${interface_name}:
      dhcp4: yes
      dhcp6: no
      nameservers:
        addresses:
%{ for name_server in name_servers ~}
          - ${name_server}
%{ endfor ~}
