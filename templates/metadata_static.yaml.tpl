local-hostname: ${hostname}
instance-id: ${instance_id}
network:
  version: 2
  ethernets:
    ${interface_name}:
      dhcp4: no
      dhcp6: no
      addresses: [${ip_addr}/${ip_netmask}]
      gateway4: ${ip_gateway}
      nameservers:
        addresses:
%{ for name_server in name_servers ~}
          - ${name_server}
%{ endfor ~}
