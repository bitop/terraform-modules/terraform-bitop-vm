// output "name" {
//   description = "VM Names"
//   value       = flatten([vsphere_virtual_machine.linux.*.name, vsphere_virtual_machine.windows.*.name, vsphere_virtual_machine.cloud-init.*.name, vsphere_virtual_machine.ignition.*.name])
// }

// output "dns_name" {
//   description = "VM Names"
//   value       = formatlist("%s.%s", flatten([vsphere_virtual_machine.linux.*.name, vsphere_virtual_machine.windows.*.name, vsphere_virtual_machine.cloud-init.*.name, vsphere_virtual_machine.ignition.*.name]), var.zone)
// }

// output "ip_addr" {
//   description = "default ip address of the deployed VM"
//   value       = flatten([vsphere_virtual_machine.linux.*.default_ip_address, vsphere_virtual_machine.windows.*.default_ip_address, vsphere_virtual_machine.cloud-init.*.default_ip_address, vsphere_virtual_machine.ignition.*.default_ip_address])
// }

// output "mac_addr" {
//   description = "all the registered ip address of the VM"
//   value       = flatten(concat(vsphere_virtual_machine.linux[*].network_interface[0].mac_address, vsphere_virtual_machine.windows[*].network_interface[0].mac_address, vsphere_virtual_machine.cloud-init[*].network_interface[0].mac_address, vsphere_virtual_machine.ignition[*].network_interface[0].mac_address))
// }

// output "cpu" {
//   description = "Vm CPU Info"
//   value = flatten([vsphere_virtual_machine.linux.*.num_cpus, vsphere_virtual_machine.windows.*.num_cpus, vsphere_virtual_machine.cloud-init.*.num_cpus, vsphere_virtual_machine.ignition.*.num_cpus])
// }

// output "ram" {
//   description = "Vm ram Info"
//   value = flatten([vsphere_virtual_machine.linux.*.memory, vsphere_virtual_machine.windows.*.memory, vsphere_virtual_machine.cloud-init.*.memory, vsphere_virtual_machine.ignition.*.memory])
// }

// output "disk" {
//   description = "Vm Disk Info"
//   value = flatten([concat(vsphere_virtual_machine.linux[*].disk[*].size, vsphere_virtual_machine.windows[*].disk[*].size, vsphere_virtual_machine.cloud-init[*].disk[*].size, vsphere_virtual_machine.ignition[*].disk[*].size)])
// }

// output "network" {
//   description = "VM Network Info"
//   value = var.network
// }

// output "id" {
//   description = "UUID of the VM in vSphere"
//   value       = flatten([vsphere_virtual_machine.linux.*.id, vsphere_virtual_machine.windows.*.id, vsphere_virtual_machine.cloud-init.*.id, vsphere_virtual_machine.ignition.*.id])
// }

// output "uuid" {
//   description = "UUID of the VM in vSphere"
//   value       = flatten([vsphere_virtual_machine.linux.*.uuid, vsphere_virtual_machine.windows.*.uuid, vsphere_virtual_machine.cloud-init.*.uuid, vsphere_virtual_machine.ignition.*.uuid])
// }

// output "dns_name_ip" {
//   value = zipmap(
//     flatten([vsphere_virtual_machine.linux.*.name, vsphere_virtual_machine.windows.*.name, vsphere_virtual_machine.cloud-init.*.name, vsphere_virtual_machine.ignition.*.name]),
//     flatten([vsphere_virtual_machine.linux.*.default_ip_address, vsphere_virtual_machine.windows.*.default_ip_address, vsphere_virtual_machine.cloud-init.*.default_ip_address, vsphere_virtual_machine.ignition.*.default_ip_address])
//   )
// }

// output "name_ip" {
//   value = zipmap(
//     flatten([vsphere_virtual_machine.linux.*.name, vsphere_virtual_machine.windows.*.name, vsphere_virtual_machine.cloud-init.*.name, vsphere_virtual_machine.ignition.*.name]),
//     flatten([vsphere_virtual_machine.linux.*.default_ip_address, vsphere_virtual_machine.windows.*.default_ip_address, vsphere_virtual_machine.cloud-init.*.default_ip_address, vsphere_virtual_machine.ignition.*.default_ip_address])
//   )
// }

// output "name_id" {
//   value = zipmap(
//     flatten([vsphere_virtual_machine.linux.*.name, vsphere_virtual_machine.windows.*.name, vsphere_virtual_machine.cloud-init.*.name, vsphere_virtual_machine.ignition.*.name]),
//     flatten([vsphere_virtual_machine.linux.*.id, vsphere_virtual_machine.windows.*.id, vsphere_virtual_machine.cloud-init.*.id, vsphere_virtual_machine.ignition.*.id])
//   )
// }
