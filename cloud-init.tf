// Cloning a Linux or Windows VM from a given template.
resource "vsphere_virtual_machine" "cloud-init" {
  count = var.is_cloud_init == true ? var.instances : 0

  depends_on = [var.vm_depends_on]
  name       = var.static_vm_name != null ? var.static_vm_name : format("${var.vm_name}-${var.vm_name_format}", count.index + 1)

  resource_pool_id        = data.vsphere_resource_pool.pool.id
  folder                  = var.vm_folder
  tags                    = var.tag_ids != null ? var.tag_ids : data.vsphere_tag.tag[*].id
  custom_attributes       = var.custom_attributes
  annotation              = var.annotation
  firmware                = var.content_library == null && var.firmware == null ? data.vsphere_virtual_machine.template[0].firmware : var.firmware
  efi_secure_boot_enabled = var.content_library == null && var.efi_secure_boot == null ? data.vsphere_virtual_machine.template[0].efi_secure_boot_enabled : var.efi_secure_boot
  enable_disk_uuid        = var.content_library == null && var.enable_disk_uuid == null ? data.vsphere_virtual_machine.template[0].enable_disk_uuid : var.enable_disk_uuid
  storage_policy_id       = var.storage_policy_id

  datastore_cluster_id = var.datastore_cluster != "" ? data.vsphere_datastore_cluster.datastore_cluster[0].id : null
  datastore_id         = var.datastore != "" ? data.vsphere_datastore.datastore[0].id : null

  num_cpus               = var.cpu_number
  num_cores_per_socket   = var.cpu_cores_per_socket
  cpu_hot_add_enabled    = var.cpu_hot_add_enabled
  cpu_hot_remove_enabled = var.cpu_hot_remove_enabled
  cpu_reservation        = var.cpu_reservation
  cpu_share_level        = var.cpu_share_level
  cpu_share_count        = var.cpu_share_level == "custom" ? var.cpu_share_count : null
  memory_reservation     = var.memory_reservation
  memory                 = var.memory_size
  memory_hot_add_enabled = var.memory_hot_add_enabled
  memory_share_level     = var.memory_share_level
  memory_share_count     = var.memory_share_level == "custom" ? var.memory_share_count : null
  guest_id               = var.content_library == null ? data.vsphere_virtual_machine.template[0].guest_id : null
  scsi_bus_sharing       = var.scsi_bus_sharing
  scsi_type              = var.scsi_type != "" ? var.scsi_type : (var.content_library == null ? data.vsphere_virtual_machine.template[0].scsi_type : null)
  scsi_controller_count = max(
    max(0, flatten([
      for item in values(var.data_disk) : [
        for elem, val in item :
        elem == "data_disk_scsi_controller" ? val : 0
    ]])...) + 1,
    ceil((max(0, flatten([
      for item in values(var.data_disk) : [
        for elem, val in item :
        elem == "unit_number" ? val : 0
    ]])...) + 1) / 15),
  var.scsi_controller)
  wait_for_guest_net_routable = var.wait_for_guest_net_routable
  wait_for_guest_ip_timeout   = var.wait_for_guest_ip_timeout
  wait_for_guest_net_timeout  = var.wait_for_guest_net_timeout

  ignored_guest_ips = var.ignored_guest_ips

  dynamic "network_interface" {
    for_each = var.network
    content {
      network_id   = data.vsphere_network.network[network_interface.key].id
      adapter_type = var.network_type != null ? var.network_type[network_interface.key] : (var.content_library == null ? data.vsphere_virtual_machine.template[0].network_interface_types[0] : null)
    }
  }
  // Disks defined in the original template
  dynamic "disk" {
    for_each = var.content_library == null ? data.vsphere_virtual_machine.template[0].disks : []
    iterator = template_disks
    content {
      label             = length(var.disk_label) > 0 ? var.disk_label[template_disks.key] : "disk${template_disks.key}"
      size              = var.disk_size_gb != null ? var.disk_size_gb[template_disks.key] : data.vsphere_virtual_machine.template[0].disks[template_disks.key].size
      unit_number       = var.scsi_controller != null ? var.scsi_controller * 15 + template_disks.key : template_disks.key
      thin_provisioned  = data.vsphere_virtual_machine.template[0].disks[template_disks.key].thin_provisioned
      eagerly_scrub     = data.vsphere_virtual_machine.template[0].disks[template_disks.key].eagerly_scrub
      datastore_id      = var.disk_datastore != "" ? data.vsphere_datastore.disk_datastore[0].id : null
      storage_policy_id = length(var.template_storage_policy_id) > 0 ? var.template_storage_policy_id[template_disks.key] : null
      io_reservation    = length(var.io_reservation) > 0 ? var.io_reservation[template_disks.key] : null
      io_share_level    = length(var.io_share_level) > 0 ? var.io_share_level[template_disks.key] : "normal"
      io_share_count    = length(var.io_share_level) > 0 && var.io_share_level[template_disks.key] == "custom" ? var.io_share_count[template_disks.key] : null
    }
  }
  // Disk for template from Content Library
  dynamic "disk" {
    for_each = var.content_library == null ? [] : [1]
    iterator = template_disks
    content {
      label       = length(var.disk_label) > 0 ? var.disk_label[template_disks.key] : "disk${template_disks.key}"
      size        = var.disk_size_gb[template_disks.key]
      unit_number = var.scsi_controller != null ? var.scsi_controller * 15 + template_disks.key : template_disks.key
      // thin_provisioned  = data.vsphere_virtual_machine.template[0].disks[template_disks.key].thin_provisioned
      // eagerly_scrub     = data.vsphere_virtual_machine.template[0].disks[template_disks.key].eagerly_scrub
      datastore_id      = var.disk_datastore != "" ? data.vsphere_datastore.disk_datastore[0].id : null
      storage_policy_id = length(var.template_storage_policy_id) > 0 ? var.template_storage_policy_id[template_disks.key] : null
      io_reservation    = length(var.io_reservation) > 0 ? var.io_reservation[template_disks.key] : null
      io_share_level    = length(var.io_share_level) > 0 ? var.io_share_level[template_disks.key] : "normal"
      io_share_count    = length(var.io_share_level) > 0 && var.io_share_level[template_disks.key] == "custom" ? var.io_share_count[template_disks.key] : null
      disk_mode         = length(var.disk_mode) > 0 ? var.disk_mode[template_disks.key] : null
    }
  }
  // Additional disks defined by Terraform config
  dynamic "disk" {
    for_each = var.data_disk
    iterator = terraform_disks
    content {
      label = terraform_disks.key
      size  = lookup(terraform_disks.value, "size_gb", null)
      unit_number = (
        lookup(
          terraform_disks.value,
          "unit_number",
          -1
          ) < 0 ? (
          lookup(
            terraform_disks.value,
            "data_disk_scsi_controller",
            0
            ) > 0 ? (
            (terraform_disks.value.data_disk_scsi_controller * 15) +
            index(keys(var.data_disk), terraform_disks.key) +
            (var.scsi_controller == tonumber(terraform_disks.value["data_disk_scsi_controller"]) ? local.template_disk_count : 0)
            ) : (
            index(keys(var.data_disk), terraform_disks.key) + local.template_disk_count
          )
          ) : (
          tonumber(terraform_disks.value["unit_number"])
        )
      )
      thin_provisioned  = lookup(terraform_disks.value, "thin_provisioned", "true")
      eagerly_scrub     = lookup(terraform_disks.value, "eagerly_scrub", "false")
      datastore_id      = lookup(terraform_disks.value, "datastore_id", null)
      storage_policy_id = lookup(terraform_disks.value, "storage_policy_id", null)
      io_reservation    = lookup(terraform_disks.value, "io_reservation", null)
      io_share_level    = lookup(terraform_disks.value, "io_share_level", "normal")
      io_share_count    = lookup(terraform_disks.value, "io_share_level", null) == "custom" ? lookup(terraform_disks.value, "io_share_count") : null
      disk_mode         = lookup(terraform_disks.value, "disk_mode", null)
    }
  }
  clone {
    template_uuid = var.content_library == null ? data.vsphere_virtual_machine.template[0].id : data.vsphere_content_library_item.library_item_template[0].id
    linked_clone  = var.linked_clone
    timeout       = var.timeout
  }

  extra_config = {
    "guestinfo.metadata" = var.use_dhcp == true ? base64encode(templatefile("${path.module}/templates/metadata_dhcp.yaml.tpl", {
      hostname       = "${var.static_vm_name != null ? var.static_vm_name : format("${var.vm_name}-${var.vm_name_format}.${var.domain}", count.index + 1)}"
      instance_id    = "${var.static_vm_name != null ? var.static_vm_name : format("${var.vm_name}-${var.vm_name_format}", count.index + 1)}"
      interface_name = var.interface_name
      name_servers   = var.dns_servers
      })) : base64encode(templatefile("${path.module}/templates/metadata_static.yaml.tpl", {
      hostname       = "${var.static_vm_name != null ? var.static_vm_name : format("${var.vm_name}-${var.vm_name_format}.${var.domain}", count.index + 1)}"
      instance_id    = "${var.static_vm_name != null ? var.static_vm_name : format("${var.vm_name}-${var.vm_name_format}", count.index + 1)}"
      interface_name = var.interface_name
      ip_addr        = var.ip_addr[count.index]
      ip_netmask     = var.ip_netmask != [] ? var.ip_netmask[0] : local.ip_netmask[0]
      ip_gateway     = var.ip_gateway != "" ? var.ip_gateway : local.ip_gateway
      name_servers   = var.dns_servers
    }))
    "guestinfo.metadata.encoding" = "base64"
    "guestinfo.userdata" = "%{if var.cloud_init_file != ""}${base64encode(file(var.cloud_init_file))}%{else}${base64encode(file("${path.module}/templates/userdata.yaml.tpl"))}%{endif}"
    "guestinfo.userdata.encoding" = "base64"
  }
  // Advanced options
  hv_mode                          = var.hv_mode
  ept_rvi_mode                     = var.ept_rvi_mode
  nested_hv_enabled                = var.nested_hv_enabled
  enable_logging                   = var.enable_logging
  cpu_performance_counters_enabled = var.cpu_performance_counters_enabled
  swap_placement_policy            = var.swap_placement_policy
  latency_sensitivity              = var.latency_sensitivity

  shutdown_wait_timeout = var.shutdown_wait_timeout
  force_power_off       = var.force_power_off

  lifecycle { ignore_changes = [clone.0.template_uuid, extra_config] }
}
