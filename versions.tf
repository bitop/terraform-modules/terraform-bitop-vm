terraform {
  required_version = ">= 0.14.0"

  required_providers {
    vsphere = {
      source  = "hashicorp/vsphere"
      version = ">= 1.26.0"
    }
  }
}
